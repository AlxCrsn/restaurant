package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import classes.Masa;
import classes.Produs;
import classesDepozit.StocImpDAOProdus;
import gui.MasaView;
import gui.RestaurantView;

public class MasaController {

	private JButton masaViewButtonRestaurantView;
	private Masa masaModel;
	private RestaurantView restaurantView;
	private MasaView masaView;
	private StocImpDAOProdus stoc;

	public MasaController(JButton masaViewButton, Masa masaModel, RestaurantView restaurantView,MasaView masaView,StocImpDAOProdus stocImp) {
		this.masaViewButtonRestaurantView = masaViewButton;
		this.masaModel = masaModel;
		this.restaurantView = restaurantView;
		this.masaView=masaView;
		this.stoc=stocImp;
		this.masaViewButtonRestaurantView.addActionListener(new clickMasaButonRestaurantView());
		this.masaView.getAdProdus().addActionListener(new clickAddProdusMasaView() );
		this.masaView.getBackToRestaurantView().addActionListener(new clickBackToRestaurantView());
		this.masaView.getTotalConsumatieButton().addActionListener(new clickTotalButtonMasaView());

	}

	
	private class clickBackToRestaurantView implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			masaView.setVisible(false);
			restaurantView.setVisible(true);
		}

	}

	private class clickMasaButonRestaurantView implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {

			// masaViewButton.setText(masaModel.getEsteOcupat().toString());
			masaView.setVisible(true);
			restaurantView.setVisible(false);
			//masaModel.setOcupat(!masaModel.getEsteOcupat());
			//masaViewButtonRestaurantView.setText(masaModel.getEsteOcupat().toString());

		}
	}

	private class clickAddProdusMasaView implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			masaModel.setOcupat(true);
			masaViewButtonRestaurantView.setText(masaModel.getEsteOcupat().toString()+masaModel.getNumar());
			Produs produs=(Produs)masaView.getProduseDepozit().getSelectedItem();
			masaModel.getListaProduseConsumatie().add(produs);
			masaView.getTextPane().setText(produs.toString());
		}

	}

	private class clickTotalButtonMasaView implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			masaModel.setOcupat(false);
			masaViewButtonRestaurantView.setText(masaModel.getEsteOcupat().toString());
		}

	}
}
