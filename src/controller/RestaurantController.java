package controller;

import java.util.List;

import classes.Masa;
import classesDepozit.StocImpDAOProdus;
import gui.MasaView;
import gui.RestaurantView;

public class RestaurantController {

	private RestaurantView restaurantView;
	private List<Masa> mese;
	private StocImpDAOProdus stoc;


	public RestaurantController(RestaurantView view, List<Masa> model, StocImpDAOProdus stoc) {
		this.restaurantView = view;
		this.mese = model;
		this.stoc =stoc;

		for (int i = 0; i < mese.size(); i++) {
			new MasaController(restaurantView.getMeseButton().get(i), mese.get(i), restaurantView,restaurantView.getMasaView().get(i),this.stoc);
			
		}

	}

}
