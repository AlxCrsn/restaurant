package classes;

import java.io.Serializable;

public class Produs implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String denumire;
	public int cantitate;
	public int pret;

	public Produs(String Nume,int cantitate, int pret) {
		this.denumire=Nume;
		this.cantitate=cantitate;
		this.pret=pret;
	}

	public void setDenumire(String strDenumire) {
		this.denumire = strDenumire;

	}

	public void setCantitate(int intCantitate) {
		this.cantitate = intCantitate;
	}

	public void setPret(int intPret) {
		this.pret = intPret;
	}

	public String getDenumire() {
		return this.denumire;
	}

	public int getPret() {
		return this.pret;
	}

	public int getCantitate() {
		return this.cantitate;
	}
	public void scadeCantitateProdus(String numeProdus, int cantitateProdus){
		
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return denumire;
	}

}
