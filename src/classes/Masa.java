package classes;

import java.util.ArrayList;
import java.util.List;

public class Masa {
	private List<Produs> listaProduseConsumatie;
	private Boolean esteOcupat;

	public Boolean getEsteOcupat() {
		return esteOcupat;
	}

	private int id;

	public Masa() {
		this.listaProduseConsumatie = new ArrayList<Produs>();
		this.esteOcupat = false;

	}

	public Masa(int id) {
		this.listaProduseConsumatie = new ArrayList<Produs>();
		this.id = id;
		this.esteOcupat = false;
	}

	public void setOcupat(Boolean boolOcupat) {
		this.esteOcupat = boolOcupat;
	}

	public void setID(int intNumar) {
		this.id = intNumar;

	}

	public int getNumar() {
		return this.id;
	}

	public List<Produs> getListaProduseConsumatie() {
		return this.listaProduseConsumatie;
	}

	public int totalConsumatieMasa() {
		int totalConsumatie = 0;
		for (Produs produsIterator : this.listaProduseConsumatie) {
			totalConsumatie += produsIterator.pret * produsIterator.cantitate;
		}
		return totalConsumatie;
	}

}
