////StocImpDao


package classesDepozit;

import java.util.ArrayList;
import java.util.List;

import classes.Produs;

public class StocImpDAOProdus implements ProdusDAO {
	static List<Produs>Stoc;
	
	public StocImpDAOProdus() {
		this.Stoc=new ArrayList<Produs>();
		Stoc.add(new Produs("Becks",80,4));
		Stoc.add(new Produs("Jidvei alb",50,8));
		Stoc.add(new Produs("Tuica de casa",6,4));
		Stoc.add(new Produs("Absolut",15,10));
		Stoc.add(new Produs("Jameson",10,15));
		Stoc.add(new Produs("Cola",100,5));
		Stoc.add(new Produs("Cafea",20,5));
		Stoc.add(new Produs("Apa plata",150,5));
		Stoc.add(new Produs("Apa minerala",150,5));
		Stoc.add(new Produs("Jidvei rosu",50,8));
		Stoc.add(new Produs("Grolsch",60,10));
		Stoc.add(new Produs("Jack Daniels",10,13));
		Stoc.add(new Produs("Timisoreana",100,6));
		Stoc.add(new Produs("Bergenbier",120,4));
		Stoc.add(new Produs("Ursus",90,5));
		Stoc.add(new Produs("Budweiser",60,13));
		Stoc.add(new Produs("Heineken",90,7));
		Stoc.add(new Produs("Peroni",60,6));
		Stoc.add(new Produs("Guinnes",40,9));
		Stoc.add(new Produs("Paulaner",60,10));
		
	}


	@Override
	public void updateProdus(Produs produs) {
		for(Produs stocIterator: Stoc){
			if(produs.denumire==stocIterator.denumire)
			{
				stocIterator.cantitate=stocIterator.cantitate-produs.cantitate;
				break;
			}
			
		}
	}

	@Override
	public void deleteProdus(String numeProdus) {
		for(Produs stocIterator: Stoc){
			if(numeProdus==stocIterator.denumire)
			{
				Stoc.remove(stocIterator);
				break;
			}
		}
		

	}

	@Override
	public List<Produs> getStoc() {
		return this.Stoc;
	}

	@Override
	public Produs getProdusByNume(String numeProdus) {
		Produs returnProdus=null;
		
		for(Produs stocIterator: Stoc){
			if(numeProdus==stocIterator.denumire)
			{
				returnProdus=stocIterator;
				break;
			}
		}
		return returnProdus;
}
}