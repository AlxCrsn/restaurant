//PRODUS DAO

package classesDepozit;
import java.util.List;

import classes.Produs;

public interface ProdusDAO {
	public List<Produs> getStoc();
	public Produs getProdusByNume(String numeProdus);
	public void updateProdus(Produs produs);
	public void deleteProdus(String numeProdus);
	

}