package gui;

import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class RestaurantView extends JFrame {

	private List<JButton> meseButton;

	private List<MasaView> masaView;

	private JPanel contentPane;

	/**
	 * Create the frame.
	 */
	public RestaurantView(int nrMese) {
		masaView = new ArrayList<MasaView>();
		meseButton = new ArrayList<JButton>();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		for (int i = 0; i < nrMese; i++) {
			JButton masaButtonView = new JButton("LIBER");
			masaButtonView.setBounds(34 + i * 100, 27, 89, 23);
			meseButton.add(masaButtonView);
			contentPane.add(masaButtonView);
			masaView.add(new MasaView(i));
		}
	}

	public void setHandler(ActionListener action) {
		for (JButton jbutton : this.meseButton) {
			jbutton.addActionListener(action);
		}

	}

	public List<JButton> getMeseButton() {
		return meseButton;
	}

	public List<MasaView> getMasaView() {
		return masaView;
	}

	public void setMasaView(List<MasaView> masaView) {
		this.masaView = masaView;
	}
}
