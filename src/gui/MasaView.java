package gui;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;

import classes.Produs;
import classesDepozit.StocImpDAOProdus;

public class MasaView extends JFrame {
	private JButton addProdusButton;
	private JPanel contentPanel;
	private JButton totalConsumatieButton;
	private JTextPane textPane;
	private JButton backToRestaurantView;
	private JComboBox<Produs> produseDepozit;
	private StocImpDAOProdus stoc;
	private int id;

	public MasaView(int id) {
		stoc = new StocImpDAOProdus();
		this.id = id;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);

		contentPanel = new JPanel();
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPanel);
		contentPanel.setLayout(null);
		addProdusButton = new JButton("AdProdus");
		addProdusButton.setBounds(20, 20, 20, 20);
		contentPanel.add(addProdusButton);
		totalConsumatieButton = new JButton("Total");
		totalConsumatieButton.setBounds(70, 80, 50, 50);
		contentPanel.add(totalConsumatieButton);
		textPane = new JTextPane();
		textPane.setBounds(150, 151, 50, 50);
		contentPanel.add(textPane);
		textPane.setEditable(false);
		backToRestaurantView = new JButton("Back");
		backToRestaurantView.setBounds(50, 50, 50, 50);
		contentPanel.add(backToRestaurantView);

		produseDepozit = new JComboBox<Produs>();
		produseDepozit.setBounds(300, 300, 150, 20);
		for (Produs pr : stoc.getStoc()) {
			produseDepozit.addItem(pr);;
		}
		contentPanel.add(produseDepozit);

	}

	public void setHandler(ActionListener action) {

		this.addProdusButton.addActionListener(action);
	}

	public JButton getAdProdus() {
		return addProdusButton;
	}

	public JButton getTotalConsumatieButton() {
		return totalConsumatieButton;
	}

	public JButton getBackToRestaurantView() {
		return backToRestaurantView;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public JComboBox<Produs> getProduseDepozit() {
		return produseDepozit;
	}

	public void setProduseDepozit(JComboBox<Produs> produseDepozit) {
		this.produseDepozit = produseDepozit;
	}

	public JTextPane getTextPane() {
		return textPane;
	}

}
