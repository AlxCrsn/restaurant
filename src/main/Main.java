package main;
import java.util.ArrayList;
import java.util.List;

import classes.Masa;
import classesDepozit.StocImpDAOProdus;
import controller.RestaurantController;
import gui.MasaView;
import gui.RestaurantView;

public class Main {

	public static void main(String[] args) {
		List<Masa> mese=new ArrayList<Masa>();
		StocImpDAOProdus stoc=new StocImpDAOProdus();
		mese.add(new Masa(1));
		mese.add(new Masa(2));
		mese.add(new Masa(3));
		mese.add(new Masa(4));
		RestaurantView view =new RestaurantView(mese.size());
		view.setVisible(true);
		new RestaurantController(view, mese,stoc);
		
	}

}
